package com.example.examenc2;

import Modelo.DefineTabla;

public class Productos {
    //Atrinutos
    private String codigo;
    private String nombre;
    private String marca;
    private float precio;
    private int perecedero;

    //Constructor
    public Productos(){
        this.codigo = "";
        this.nombre = "";
        this.marca = "";
        this.precio = 0.0F;
        this.perecedero = 0;
    }

    //Constructor
    public Productos(String codigo, String nombre, String marca, float precio, int perecedero){
        this.codigo = codigo;
        this.nombre = nombre;
        this.marca = marca;
        this.precio = precio;
        this.perecedero = perecedero;
    }

    //Encapsulamiento


    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getPerecedero() {
        return perecedero;
    }

    public void setPerecedero(int perecedero) {
        this.perecedero = perecedero;
    }
}
