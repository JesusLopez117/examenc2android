package com.example.examenc2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import Modelo.ProductosDb;

public class EditarActivity extends AppCompatActivity {

    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioButton btnPerecedero;
    private RadioButton btnNoPerecedero;
    private Button btnActualizar;
    private Button btnBorrar;

    private Button btnCerrar;

    private Button btnBuscar;

    private Productos productos;

    static ProductosDb productosDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);
        iniciarComponentes();

        productosDb = new ProductosDb(getApplicationContext());
        productosDb.openDataBase();

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                borrar();
            }
        });

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actualizar();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrar();
            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buscar();
            }
        });
    }

    private void buscar() {
        if(validarExistente()){
            productosDb = new ProductosDb(getApplicationContext());
            productos = new Productos();

            productos = productosDb.getProducto(txtCodigo.getText().toString());

            this.txtCodigo.setText(productos.getCodigo());
            this.txtNombre.setText(productos.getNombre());
            this.txtMarca.setText(productos.getMarca());
            this.txtPrecio.setText(String.valueOf(productos.getPrecio()));
            if(productos.getPerecedero() == 1){
                btnPerecedero.setSelected(true);
            }else{
                btnNoPerecedero.setSelected(true);
            }
        }else{
            Toast.makeText(getApplicationContext(), "El producto no existe ",Toast.LENGTH_SHORT).show();

        }
    }

    private void cerrar() {
        AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
        dialogo.setTitle("Tienda de Productos");
        dialogo.setMessage(" ¿Desea cerrar? ");

        //Confirmación
        dialogo.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        //Negación
        dialogo.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialogo.show();
    }

    private void actualizar() {


        if(validar()){

            productos = new Productos();
            productos.setCodigo(txtCodigo.getText().toString());
            productos.setNombre(txtNombre.getText().toString());
            productos.setMarca(txtMarca.getText().toString());
            productos.setPrecio(Float.parseFloat(txtPrecio.getText().toString()));

            //Validar cual opcion eligio.
            if(btnPerecedero.isChecked()){
                productos.setPerecedero(1);
            }else{
                productos.setPerecedero(0);
            }
            productosDb.updateProducto(productos);
            Toast.makeText(getApplicationContext(), "Se modifico con exito ",Toast.LENGTH_SHORT).show();
            setResult(Activity.RESULT_OK);
            limpiar();
        }else{
            Toast.makeText(getApplicationContext(), "Falta capturar datos",Toast.LENGTH_SHORT).show();

        }
    }

    private void borrar() {
        if(txtCodigo.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "Falta capturar el codigo",Toast.LENGTH_SHORT).show();

        }else{
            productosDb = new ProductosDb(getApplicationContext());

            productosDb.deleteProducto(txtCodigo.getText().toString());
            Toast.makeText(getApplicationContext(), "Se elimino con exito ",Toast.LENGTH_SHORT).show();
            setResult(Activity.RESULT_OK);
            limpiar();
        }
    }

    private boolean validarExistente() {
        productosDb = new ProductosDb(getApplicationContext());

        boolean productoExistente = productosDb.buscarProdcuto(txtCodigo.getText().toString());
        productosDb.openDataBase();

        return productoExistente;
    }

    private void iniciarComponentes() {
        txtCodigo = findViewById(R.id.txtCodigo);
        txtNombre = findViewById(R.id.txtNombre);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        btnPerecedero = findViewById(R.id.btnPerecedero);
        btnNoPerecedero = findViewById(R.id.btnNoPerecedero);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnCerrar = findViewById(R.id.btnCerrar);
        btnBuscar = findViewById(R.id.btnBuscar);
    }

    private void limpiar() {
        txtNombre.setText("");
        txtCodigo.setText("");
        txtMarca.setText("");
        txtPrecio.setText("");
    }

    private boolean validar() {
        if(txtCodigo.getText().toString().equals("")) return false;
        if(txtNombre.getText().toString().equals("")) return false;
        if(txtMarca.getText().toString().equals("")) return false;
        if(txtPrecio.getText().toString().equals("")) return false;

        return true;
    }
}