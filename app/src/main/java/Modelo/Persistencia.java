package Modelo;

import com.example.examenc2.Productos;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertProducto(Productos productos);
    public long updateProducto(Productos productos);
    public void deleteProducto(String codigo);
}
