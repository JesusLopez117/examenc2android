package Modelo;

import android.database.Cursor;

import com.example.examenc2.Productos;

import java.util.ArrayList;

public interface Proyeccion {
    public Productos getProducto(String codigo);
    public Productos readProducto(Cursor cursor);

    public boolean buscarProdcuto(String codigo);
}
