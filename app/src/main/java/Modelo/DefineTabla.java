package Modelo;

public class DefineTabla {

    public DefineTabla(){}

    public static abstract class Productos{
        //Variables necesarias
        public static final String TABLE_NAME = "productos";
        public static final String COLUMN_NAME_CODIGO = "codigo";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        public static final String COLUMN_NAME_MARCA = "marca";
        public static final String COLUMN_NAME_PRECIO = "precio";
        public static final String COLUMN_NAME_PERECEDERO = "perecedero";
    }

    //Objeto para los resgistros.
    public static String[] RESGITROS = new String[]{
            Productos.COLUMN_NAME_CODIGO,
            Productos.COLUMN_NAME_NOMBRE,
            Productos.COLUMN_NAME_MARCA,
            Productos.COLUMN_NAME_PRECIO,
            Productos.COLUMN_NAME_PERECEDERO
    };

}
