package Modelo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ProductosDbHelper extends SQLiteOpenHelper {
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String FLOAT_TYPE = " FLOAT";
    private static final String COMA_SEP = " ,";

    private static final String SQL_CREATE_PRODUCTO = "CREATE TABLE " +
            DefineTabla.Productos.TABLE_NAME + " (" +
            DefineTabla.Productos.COLUMN_NAME_CODIGO + TEXT_TYPE + COMA_SEP +
            DefineTabla.Productos.COLUMN_NAME_NOMBRE + TEXT_TYPE + COMA_SEP +
            DefineTabla.Productos.COLUMN_NAME_MARCA + TEXT_TYPE + COMA_SEP +
            DefineTabla.Productos.COLUMN_NAME_PRECIO + FLOAT_TYPE + COMA_SEP +
            DefineTabla.Productos.COLUMN_NAME_PERECEDERO + INTEGER_TYPE + ")";

    private static final String SQL_DELETE_PRODUCTOS = "DROP TABLE IF EXISTS " +
            DefineTabla.Productos.TABLE_NAME;

    private static final String DATABASE_NAME = "sistema.db";

    private static final int DATABASE_VERCION = 1;


    public ProductosDbHelper (Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERCION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PRODUCTO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_PRODUCTOS);
        onCreate(db);
    }
}
