package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.examenc2.Productos;

public class ProductosDb implements Persistencia, Proyeccion{
    //Variables necesarias
    private Context context;
    private ProductosDbHelper helper;
    private SQLiteDatabase db;

    //Constructor
    public ProductosDb(Context context, ProductosDbHelper helper){
        this.context = context;
        this.helper = helper;
    }

    public ProductosDb(Context context){
        this.context = context;
        this.helper = new ProductosDbHelper(this.context);
    }

    @Override
    public void openDataBase() {
        db = helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    @Override
    public long insertProducto(Productos productos) {

        //Creación del contenedor de los datos
        ContentValues values = new ContentValues();

        values.put(DefineTabla.Productos.COLUMN_NAME_CODIGO, productos.getCodigo());
        values.put(DefineTabla.Productos.COLUMN_NAME_NOMBRE, productos.getNombre());
        values.put(DefineTabla.Productos.COLUMN_NAME_MARCA, productos.getMarca());
        values.put(DefineTabla.Productos.COLUMN_NAME_PRECIO, productos.getPrecio());
        values.put(DefineTabla.Productos.COLUMN_NAME_PERECEDERO, productos.getPerecedero());

        this.openDataBase();

        long num = db.insert(DefineTabla.Productos.TABLE_NAME, null, values);

        Log.d("Agregar", "Se inserto un alaumno" + num);
        return num;
    }

    @Override
    public long updateProducto(Productos productos) {
        //Creación del contenedor de los datos
        ContentValues values = new ContentValues();

        values.put(DefineTabla.Productos.COLUMN_NAME_CODIGO, productos.getCodigo());
        values.put(DefineTabla.Productos.COLUMN_NAME_NOMBRE, productos.getNombre());
        values.put(DefineTabla.Productos.COLUMN_NAME_MARCA, productos.getMarca());
        values.put(DefineTabla.Productos.COLUMN_NAME_PRECIO, productos.getPrecio());
        values.put(DefineTabla.Productos.COLUMN_NAME_PERECEDERO, productos.getPerecedero());

        this.openDataBase();

        long num = db.update(
                DefineTabla.Productos.TABLE_NAME,
                values,
                DefineTabla.Productos.COLUMN_NAME_CODIGO + " = " + productos.getCodigo(),
                null
        );

        return num;

    }

    @Override
    public void deleteProducto(String codigo) {
        this.openDataBase();

        db.delete(
                DefineTabla.Productos.TABLE_NAME,
                DefineTabla.Productos.COLUMN_NAME_CODIGO + "=?",
                new String[] {String.valueOf(codigo)}
        );
    }

    @Override
    public Productos getProducto(String codigo) {

        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTabla.Productos.TABLE_NAME,
                DefineTabla.RESGITROS,
                DefineTabla.Productos.COLUMN_NAME_CODIGO + " = ?",
                new String[] {codigo},
                null, null, null
        );

        cursor.moveToFirst();
        Productos productos = readProducto(cursor);
        return productos;
        //Regresara el producto

    }

    @Override
    public boolean buscarProdcuto(String codigo) {
        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTabla.Productos.TABLE_NAME,
                DefineTabla.RESGITROS,
                DefineTabla.Productos.COLUMN_NAME_CODIGO + " = ?",
                new String[] {codigo},
                null, null, null
        );

        return cursor.moveToFirst();
        //Regresara true si encuentra algun codigo igual
    }

    @Override
    public Productos readProducto(Cursor cursor) {
        Productos productos = new Productos();

        productos.setCodigo(cursor.getString(0));
        productos.setNombre(cursor.getString(1));
        productos.setMarca(cursor.getString(2));
        productos.setPrecio(cursor.getFloat(3));
        productos.setPerecedero(cursor.getInt(4));

        return productos;
    }


}
