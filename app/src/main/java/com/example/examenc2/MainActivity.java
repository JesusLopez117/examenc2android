package com.example.examenc2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import Modelo.ProductosDb;

public class MainActivity extends AppCompatActivity {

    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioButton btnPerecedero;
    private RadioButton btnNoPerecedero;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnEditar;
    private Button btnNuveo;

    private Productos productos;

    static ProductosDb productosDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        productosDb = new ProductosDb(getApplicationContext());
        productosDb.openDataBase();

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardar();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editar();
            }
        });

        btnNuveo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nuevo();
            }
        });
    }

    private void nuevo() {
    }

    private void editar() {
        Intent intent = new Intent(MainActivity.this, EditarActivity.class);
        startActivity(intent);
    }

    private void limpiar() {
        txtNombre.setText("");
        txtCodigo.setText("");
        txtMarca.setText("");
        txtPrecio.setText("");
    }

    private void guardar() {

        if(validar()){
            productos = new Productos();
            productos.setCodigo(txtCodigo.getText().toString());
            productos.setNombre(txtNombre.getText().toString());
            productos.setMarca(txtMarca.getText().toString());
            productos.setPrecio(Float.parseFloat(txtPrecio.getText().toString()));

            //Validar cual opcion eligio.
            if(btnPerecedero.isChecked()){
                productos.setPerecedero(1);
            }else{
                productos.setPerecedero(0);
            }
            productosDb = new ProductosDb(getApplicationContext());
            if(validarExistente()){
                productosDb.updateProducto(productos);
                Toast.makeText(getApplicationContext(), "Se modifico con exito ",Toast.LENGTH_SHORT).show();
                setResult(Activity.RESULT_OK);
                limpiar();
            }else{
                productosDb.insertProducto(productos);
                Toast.makeText(getApplicationContext(), "Se inserto un produto ",Toast.LENGTH_SHORT).show();
                setResult(Activity.RESULT_OK);
                limpiar();
            }
        }else{
            Toast.makeText(getApplicationContext(), "Falto capturar datos ", Toast.LENGTH_SHORT).show();
        }

    }

    private boolean validarExistente() {
        productosDb = new ProductosDb(getApplicationContext());

        boolean productoExistente = productosDb.buscarProdcuto(txtCodigo.getText().toString());
        productosDb.openDataBase();

        return productoExistente;
    }

    private boolean validar() {
        if(txtCodigo.getText().toString().equals("")) return false;
        if(txtNombre.getText().toString().equals("")) return false;
        if(txtMarca.getText().toString().equals("")) return false;
        if(txtPrecio.getText().toString().equals("")) return false;

        return true;
    }

    private void iniciarComponentes() {
        txtCodigo = findViewById(R.id.txtCodigo);
        txtNombre = findViewById(R.id.txtNombre);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        btnPerecedero = findViewById(R.id.btnPerecedero);
        btnNoPerecedero = findViewById(R.id.btnNoPerecedero);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnEditar = findViewById(R.id.btnEditar);
        btnNuveo = findViewById(R.id.btnNuevo);
    }
}